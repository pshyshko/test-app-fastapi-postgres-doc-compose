# test-app-fastapi-postgres-doc-compose
## Kluczowe technologie
- FastAPI - asynchroniczny web framework.
- Pydantic - walidacja danych.
- databases - biblioteka do wsparcia asynchronicznego.
- SQLAlchemy 
- Postgresql
- Docker
- Docker-compose

## Instalacja
Do zainstalowania aplikacji wymagany jest [Docker](https://docs.docker.com/docker-for-windows/install/). 
Trzeba uruchomić dockera i upewnić się, że maszyna dockerowa jest uruchomiona.
```
git clone https://gitlab.com/pshyshko/test-app-fastapi-postgres-doc-compose.git
cd test-app-fastapi-postgres-doc-compose
docker-compose -f docker-compose.prod.yml up --build
# or
docker-compose -f docker-compose.dev.yml up --build
```

W pliku docker-compose.yml można skonfigurować wszystkie zmienne środowiskowe.
## Dokumentacja
W FastAPI dokumentacja jest generowana automatycznie i pobiera opis endpointów z wewnętrznej dokumentacji (docstring, opis modelu). Znajduje się tam również opis wszystkich użytych modeli oraz możliwość ręcznego testowania wszystkich endpointów, przykłady zapytań.

Pod tym adresem znajduje się dokumentacja:
**Uwaga! Jest dostępna po uruchomieniu aplikacji.**
```
http://fastapi.localhost
http://fastapi.localhost/docs
```
