from pydantic import BaseModel
from datetime import datetime
import uuid


class CarIn(BaseModel):
    make: str
    color: str
    production_year: int
    avg_fuel_consumption_per_100km: float
    max_passengers: int

    class Config:
        schema_extra = {
            "example": {
                "make": "Mercedes",
                "color": "#FF0000",
                "production_year": 2019,
                "avg_fuel_consumption_per_100km": 8.5,
                "max_passengers": 5,
            }
        }


class Car(BaseModel):
    id: int
    uuid: uuid.UUID
    created_at: datetime
    make: str
    color: str
    production_year: int
    avg_fuel_consumption_per_100km: float
    max_passengers: int

    class Config:
        schema_extra = {
            "example": {
                "id": 12,
                "uuid": "8e0c2686-ba30-4ace-a765-554191d00394",
                "created_at": "2021-04-08T11:36:52.271640",
                "make": "Mercedes",
                "color": "#FF0000",
                "production_year": 2019,
                "avg_fuel_consumption_per_100km": 8.5,
                "max_passengers": 5,
            }
        }
