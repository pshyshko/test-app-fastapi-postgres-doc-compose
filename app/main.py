from fastapi import FastAPI, HTTPException, status
from typing import List
from app.models import Car, CarIn
from app.database import cars, database
import uuid
from sqlalchemy.sql import literal_column

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/api/car", response_model=List[Car], tags=["Car"], status_code=status.HTTP_200_OK)
async def get_cars():
    """
    Uzyskać listę wszystkich samochodów w bazie danych.
    """
    try:
        query = cars.select()
        items = await database.fetch_all(query)
        return items
    except HTTPException as err:
        return err(status_code=404, detail="W bazie danych nie ma ani jednego samochodu.")


@app.post("/api/car", response_description="Samochód stworzy", tags=["Car"], status_code=status.HTTP_201_CREATED)
async def create_car(car: CarIn):
    """
    Dodać nowy samochód do bazy danych.
    - **make**: marka maszyny
    - **color**: kolor w HEX #FF0000
    - **production_year**: rok produkcji
    - **avg_fuel_consumption_per_100km**: zużycie paliwa na sto kilometrów
    - **max_passengers**: maksymalna liczba pasażerów.
    """
    try:
        query = cars.insert().values(
            make=car.make, color=car.color,
            production_year=car.production_year,
            avg_fuel_consumption_per_100km=car.avg_fuel_consumption_per_100km,
            max_passengers=car.max_passengers
        )
        last_id = await database.execute(query)
        return {"status": "created", "id": last_id, **car.dict()}
    except HTTPException as err:
        return err(status_code=400, detail="Maszyna nie utworzyła.")


@app.get("/api/car/{uuid}", response_model=Car, tags=["Car"], status_code=status.HTTP_200_OK)
async def get_car_by_uuid(uuid: uuid.UUID):
    """
    Uzyskać informacje o konkretnym samochodzie za pomocą ID.
    """
    try:
        query = cars.select().where(cars.c.uuid == uuid)
        item = await database.fetch_one(query)
        return item
    except HTTPException as err:
        return err(status_code=404, detail="Nie ma takiego samochodu.")


@app.put("/api/car/{uuid}", status_code=status.HTTP_200_OK, tags=['Car'])
async def put_car_by_uuid(uuid: uuid.UUID, car: CarIn):
    """
    Update parametrów pola maszyny przez uuid
    """
    try:
        query = cars.update().where(cars.c.uuid == uuid).values(car.dict())
        respond = await database.fetch_one(query)
        return respond
    except HTTPException as err:
        return err(status_code=404, detail="Nie ma takiego samochodu.")


@app.delete("/api/car/{uuid}", status_code=status.HTTP_204_NO_CONTENT, tags=['Car'])
async def delete_car_by_uuid(uuid: uuid.UUID):
    """
    Usunąć maszynę przez uuid
    """
    try:
        query = cars.delete().where(cars.c.uuid == uuid)
        respond = await database.fetch_one(query)
        return respond
    except HTTPException as err:
        return err(status_code=404, detail="Nie ma takiego samochodu.")