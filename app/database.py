from sqlalchemy import Table, Column, Integer, String, Float
from fastapi_utils.guid_type import GUID, GUID_SERVER_DEFAULT_POSTGRESQL
import databases
import sqlalchemy
import os


DB_USERNAME = os.getenv("DB_USERNAME")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("DB_NAME")

SQLALCHEMY_DATABASE_URL = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
    DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME
    )
database = databases.Database(SQLALCHEMY_DATABASE_URL)
metadata = sqlalchemy.MetaData()


cars = Table(
    "cars",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("uuid", GUID, unique=True,
           server_default=GUID_SERVER_DEFAULT_POSTGRESQL),
    Column(
        "created_at", sqlalchemy.DateTime(timezone=False),
        server_default=sqlalchemy.func.now()),
    Column("make", String),
    Column("color", String),
    Column("production_year", Integer),
    Column("avg_fuel_consumption_per_100km", Float),
    Column("max_passengers", Integer)
    )


engine = sqlalchemy.create_engine(
    SQLALCHEMY_DATABASE_URL
)

metadata.create_all(engine)
